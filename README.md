# apwfile : Go package for AMX Netlinx .apw files

## Overview [![GoDoc](https://godoc.org/bitbucket.org/solo_works/apwfile?status.svg)](https://godoc.org/bitbucket.org/solo_works/apwfile)

This package allows reading, manipulation and creation of AMX (Harman) Netlinx  Studio workspace (.apw) files. This is used by various internal tools and is in development as required.

It was created to provide an easy way to package and edit workspaces with go based scripting tools.

## Install

```
go get bitbucket.org/solo_works/apwfile
```

## Author

Created by Sam Shelton for Solo Works London
